# Haderoth

Upgraded Blank template for Unreal Engine

![Custom splash when opening project in Unreal Engine 4](https://i.imgur.com/32ICUFr.png)

Features:

1. Git setup by [Project Borealis](https://github.com/ProjectBorealis/PBCore).
	1. Config, ignores, attributes, hooks, commit message framework/template, aliases for cli.
	1. Branches model with Status branches support in editor-only module.
1. Editor config. [Unreal Naming Convention Checker](https://devblogs.microsoft.com/visualstudio/unleashing-the-power-of-visual-studio-2022-for-c-game-development/#code-analysis-for-unreal-engine-naming-convention-checker) for Visual Studio.
1. Default plugins pack:
	1. [UEGitPlugin](https://github.com/ProjectBorealis/UEGitPlugin)
	1. [Visual Studio Tools](https://aka.ms/vc-ue-extensions)
1. Editor and Game modules organization from [Lyra Starter Game](https://unrealengine.com/marketplace/en-US/learn/lyra).

## How to use

The template is pre-made to utilize Unreal Engine's standard functionality of creating projects from templates.

* Clone to your Unreal Engine Templates folder. Migrate to newer Engine version if needed. Create project with it.

  ![Selecting Fundament project template in Unreal Engine](https://i.imgur.com/39hH0FS.png)

  The git settings will not be saved. Use e.g. [UEScripts](https://github.com/sinbad/UEScripts) to setup.

or

* Fork and rename project. I suggest [Renom](https://github.com/UnrealisticDev/Renom) for that. Rename:
	* project `Fundament`
	* modules `FundamentGame`, `FundamentEditor`
	* targets `FundamentGame`, `FundamentEditor`

* Migrate to newer Engine version if needed.

* Update all submodules to latest versions.

```bash
git submodule update --remote
```

## Contributing

The project follows the [Epic Coding Standard](https://docs.unrealengine.com/latest/en-US/epic-cplusplus-coding-standard-for-unreal-engine/). Make an issue if it doesn't.
