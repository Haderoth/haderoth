// Copyright Epic Games, Inc. All Rights Reserved.

#include "HaderothEditor.h"

#define LOCTEXT_NAMESPACE "HaderothEditor"

DEFINE_LOG_CATEGORY(LogHaderothEditor);

class FHaderothEditorModule : public FDefaultGameModuleImpl
{
    typedef FHaderothEditorModule ThisClass;
};

IMPLEMENT_MODULE(FHaderothEditorModule, HaderothEditor);

#undef LOCTEXT_NAMESPACE