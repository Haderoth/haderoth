// Copyright Epic Games, Inc. All Rights Reserved.

#include "HaderothEditorEngine.h"
#include "ISourceControlModule.h"
#include "ISourceControlProvider.h"

class IEngineLoop;

#define LOCTEXT_NAMESPACE "HaderothEditor"

UHaderothEditorEngine::UHaderothEditorEngine(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
}

void UHaderothEditorEngine::Init(IEngineLoop* InEngineLoop)
{
	Super::Init(InEngineLoop);

	// Register state branches
	const ISourceControlModule& SourceControlModule = ISourceControlModule::Get();
	{
		ISourceControlProvider& SourceControlProvider = SourceControlModule.GetProvider();
		// Order matters. Lower values are lower in the hierarchy, i.e., changes from higher branches get automatically merged down.
		// (Automatic merging requires an appropriately configured CI pipeline)
		// With this paradigm, the higher the branch is, the stabler it is, and has changes manually promoted up.
		const TArray<FString> Branches{ "origin/release", "origin/stable", "origin/promoted", "origin/trunk", "origin/main" };
		SourceControlProvider.RegisterStateBranches(Branches, TEXT("Content"));
	}
}

void UHaderothEditorEngine::Start()
{
	Super::Start();
}

void UHaderothEditorEngine::Tick(float DeltaSeconds, bool bIdleMode)
{
	Super::Tick(DeltaSeconds, bIdleMode);
}

#undef LOCTEXT_NAMESPACE
