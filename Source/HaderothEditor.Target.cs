// Copyright Denis Xalagor Petrov. MIT Licensed.

using System.Collections.Generic;
using UnrealBuildTool;

public class HaderothEditorTarget : TargetRules
{
    public HaderothEditorTarget(TargetInfo Target) : base(Target)
    {
        Type = TargetType.Editor;
        DefaultBuildSettings = BuildSettingsVersion.V2;

        ExtraModuleNames.AddRange(new string[] { "HaderothGame", "HaderothEditor" });
    }
}
