// Copyright Epic Games, Inc. All Rights Reserved.

#include "HaderothGameEngine.h"

class IEngineLoop;

UHaderothGameEngine::UHaderothGameEngine(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
}

void UHaderothGameEngine::Init(IEngineLoop* InEngineLoop)
{
	Super::Init(InEngineLoop);
}

