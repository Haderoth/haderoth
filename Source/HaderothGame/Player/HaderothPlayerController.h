// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "GameFramework/PlayerController.h"
#include "HaderothPlayerController.generated.h"

/**
 * The base player controller class used by this project.
 */
UCLASS(
	Config = Game,
	Meta = (
		ShortTooltip = "The base player controller class used by this project."
		)
)
class HADEROTHGAME_API AHaderothPlayerController : public APlayerController
{
	GENERATED_BODY()

	void Tick(float DeltaSeconds) override;

	bool bShowLogWindow = true;
};
