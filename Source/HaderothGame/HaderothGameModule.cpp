// Copyright Denis Xalagor Petrov. MIT Licensed.

#include "Modules/ModuleManager.h"

/**
 * FHaderothGameModule
 */
class FHaderothGameModule : public FDefaultGameModuleImpl
{
	virtual void StartupModule() override
	{
	}

	virtual void ShutdownModule() override
	{
	}
};

IMPLEMENT_PRIMARY_GAME_MODULE(FHaderothGameModule, HaderothGame, "HaderothGame");
