// Copyright Epic Games, Inc. All Rights Reserved.

#include "HaderothViewportClient.h"

#include "CommonUISettings.h"
#include "ICommonUIModule.h"
#include "UObject/NameTypes.h"

class UGameInstance;

UHaderothViewportClient::UHaderothViewportClient()
	: Super(FObjectInitializer::Get())
{
}

void UHaderothViewportClient::Init(struct FWorldContext& WorldContext, UGameInstance* OwningGameInstance, bool bCreateNewAudioDevice)
{
	Super::Init(WorldContext, OwningGameInstance, bCreateNewAudioDevice);
}
