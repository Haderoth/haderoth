// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CommonGameViewportClient.h"
#include "UObject/UObjectGlobals.h"

#include "HaderothViewportClient.generated.h"

class UGameInstance;
class UObject;

UCLASS(BlueprintType)
class UHaderothViewportClient : public UCommonGameViewportClient
{
	GENERATED_BODY()

public:
	UHaderothViewportClient();

	virtual void Init(struct FWorldContext& WorldContext, UGameInstance* OwningGameInstance, bool bCreateNewAudioDevice = true) override;
};
