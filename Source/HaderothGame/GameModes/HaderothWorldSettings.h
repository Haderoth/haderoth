// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/WorldSettings.h"
#include "HaderothWorldSettings.generated.h"

/**
 * The default world settings object, used primarily to set the default gameplay experience to use when playing on this map
 * This class is used by setting 'WorldSettingsClass' in DefaultEngine.ini.
 */
UCLASS()
class HADEROTHGAME_API AHaderothWorldSettings : public AWorldSettings
{
	GENERATED_BODY()

public:

	AHaderothWorldSettings(const FObjectInitializer& ObjectInitializer);
};
