// Copyright Denis Xalagor Petrov. MIT Licensed.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "HaderothGameMode.generated.h"

/**
 *
 */
UCLASS()
class HADEROTHGAME_API AHaderothGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

};
