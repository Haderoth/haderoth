// Copyright Denis Xalagor Petrov. MIT Licensed.

using System.Collections.Generic;
using UnrealBuildTool;

public class HaderothGameTarget : TargetRules
{
    public HaderothGameTarget(TargetInfo Target) : base(Target)
    {
        Type = TargetType.Game;
        DefaultBuildSettings = BuildSettingsVersion.V2;

        ExtraModuleNames.AddRange(new string[] { "HaderothGame" });
    }
}
